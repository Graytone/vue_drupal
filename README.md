## Vue.js frontend with Nuxt.js
## Drupal 8 backend with jsonapi

## TLDR
```
cd api
composer install
cd ../app
yarn
cd ../
bundle install
./bin/server
```
______

Прелесть `drupal` в том, что это комбайн, с которым один человек может осилить непомерный груз фулстек разработки веб ресурса.

В этом обзоре я рассмотрю установку каркаса проекта с акцентом на удобства разработчика.

Выбрав ЯП, принимаем соответствующий менеджер зависимостей.

В этом примере применяются: `php`, `js`, `ruby`

Следовательно, используем `composer`, `npm`, `bundler`

Руби используем с одной целью, пакет foreman позволит запустить в одном сеансе наши локальные сервера разработчиков.

Цель: локальная среда для фулстек разработки на базе `drupal8` и`vue.js`

Создадим каталог проекта

```
mkdir application
```

Сфокусируем внимание на построении каркаса приложения.

Первой командой соберем фронтенд

```
vue init nuxt/starter app
```

Второй командой соберем бэкенд

```
composer create-project drupal-composer/drupal-project:8.x-dev api --stability dev --no-interaction
```

Создадим Gemfile и укажем в нем `foreman`

```
# Gemfile
gem 'foreman'
```

Установим

```
bundle install
```

Настроим

```
# Procfile.dev
vue: npm --prefix $(pwd)/app run dev
drupal: drush rs --root=$(pwd)/api/web
```

Теперь у нас есть возможность запуска обоих приложений:

```
foreman start -f Procfile.dev
```

Создадим скрипт обертку для этой команды

```
mkdir bin
touch bin/server
echo "foreman start -f Procfile.dev" > bin/server
chmod +x bin/server
```

Запуск

```
./bin/server
```

Для обслуживания приложений приходиться перемещаться в их корневые каталоги.

Например для добавления в проект `axios`:

```
cd app
yarn add axios
```

Или, для добавления модулей и установки `drupal`:

```
cd ../api
composer require jsonapi simple_oauth devel
cd web
drush si --db-url=sqlite://sites/default/files/.ht.sqlite
```

В итоге получаем полное разделение:
отдельное серверное приложение на базе `drupal 8` организует `api` для доступа клиентского приложения к данным,
отдельное клиентское приложение на базе `nuxt.js` обеспечивает интерфейс пользователя.

Документация для работы со стеком `nuxt.js` `drupal8`:

* https://babeljs.io/learn-es2015/
* https://ru.vuejs.org/v2/guide/
* https://ru.nuxtjs.org/guide
* https://github.com/mzabriskie/axios
* https://github.com/drupal-composer/drupal-project
* https://www.youtube.com/watch?v=--ZL3EAhnwc&list=PLZOQ_ZMpYrZsyO-3IstImK1okrpfAjuMZ
* https://www.youtube.com/watch?v=rTcC0maPLSA&list=PLZOQ_ZMpYrZtqy5-o7KoDhM3n6M0duBjX
